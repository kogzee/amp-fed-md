module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        sassFiles: 'app/**/*.scss',

        sass: {
            dist: {
                files: {
                    'build/site.css': 'app/site.scss'
                }
            }
        },

        watch: {
            sass: {
                tasks: ['sass'],
                files: 'app/**/*.scss'
            }

        },

        copy: {
            bower: {
                cwd: 'bower_components',
                src: ['**'],
                dest: 'build/bower_components',
                expand: true
            },
            images: {
                cwd: 'img',
                src: ['**'],
                dest: 'build/img',
                expand: true
            },
            controllers: {
                cwd: 'app/controllers',
                src: ['**'],
                dest: 'build/app/controllers',
                expand: true
            },
            services: {
                cwd: 'app/services',
                src: ['**'],
                dest: 'build/app/services',
                expand: true
            },
            views: {
                cwd: 'app/views',
                src: ['**'],
                dest: 'build/app/views',
                expand: true
            },
            app: {
                src: 'app/app.js',
                dest: 'build/app/app.js'
            },
            mainPage: {
                src: 'index.html',
                dest: 'build/index.html'
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass');


    grunt.registerTask('dev', ['default', 'watch']);
    grunt.registerTask('default', ['sass', 'copy']);

};