var app = angular.module('ampApp',
    ['ngRoute']
);

app.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider
        .when('/people', {
            templateUrl: 'app/views/people.html',
            controller: 'peopleController'
        }).when('/', {
            templateUrl: 'app/views/people.html',
            controller: 'peopleController'
        }).otherwise({
            redirectTo: '/'
        });
}]);

app.run(['$rootScope', function ($rootScope) {

}]);