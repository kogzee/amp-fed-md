app.controller('peopleController', ['$scope', '$http','peopleService',
    function ($scope, $http, peopleService) {
        $scope.searchPeople = '';     // set the default search/filter term

        $scope.getAllPeople = function () {
            peopleService.getAllPeople().then(function (people) {
                $scope.people = people;
            });
        };
    }]);