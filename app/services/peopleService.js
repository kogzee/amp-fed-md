app.factory('peopleService', ['$http', '$q', function ($http, $q) {
    return {
        getAllPeople: function () {
            //in a real app we would call a REST service here with $http.get instead of hard coding the sample data
            var people = [
                {
                    'id': '1',
                    'firstName': 'Sean',
                    'lastName': 'Kerr',
                    'picture': 'img/sean.png',
                    'Title': 'Senior Developer'
                },
                {
                    'id': '2',
                    'firstName': 'Yaw',
                    'lastName': 'Ly',
                    'picture': 'img/yaw.png',
                    'Title': 'AEM Magician'
                },
                {
                    'id': '3',
                    'firstName': 'Lucy',
                    'lastName': 'Hehir',
                    'picture': 'img/lucy.png',
                    'Title': 'Scrum master'
                },
                {
                    'id': '4',
                    'firstName': 'Rory',
                    'lastName': 'Elrick',
                    'picture': 'img/rory.png',
                    'Title': 'AEM Guru'
                },
                {
                    'id': '5',
                    'firstName': 'Eric',
                    'lastName': 'Kwok',
                    'picture': 'img/eric.png',
                    'Title': 'Technical Lead'
                },
                {
                    'id': '6',
                    'firstName': 'Hayley',
                    'lastName': 'Crimmins',
                    'picture': 'img/hayley.png',
                    'Title': 'Dev Manager'
                }
            ];

            //return the data as a promise since that is the way it will come back once implemented as a REST service
            var deferred = $q.defer();
            deferred.resolve(people);
            return deferred.promise;
        }
    };
}]);

