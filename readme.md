# AMP code test

This test is designed to asses your skill level. 

Whilst We know you can see activity from other users, we feel that it's up to you, not to plagiarise the work of another person. 


## The test should only take a maximum of 2 hours, don't spend any longer!

## We are looking at the following skills:

- Responsive design
- Sass / css
- Semantics
- Accessibility
- JavaScript
- AngularJS
- Automated testing of JavaScript
- Git
- Grunt or Gulp (we use gulp here)

## The end result should be:

Simple, testable & well formatted code.

## Stuff you can leave out

- minification & concatenation
- cross browser support
- end to end testing

## Functional requirements

1. We are wanting you to display a list of users with a filter. Preferably, the list will update whilst the user is typing.
The page should be responsive. A creative has been supplied for the larger views, however, for the smaller views you'll need to use your judgement.

2. The final result should contain a mixin
3. Demonstrate good semantic markup

## testing

- create a couple of tests, no more than that. 

## Pre requisites

- node
- npm
- grunt

## Getting started

1. Fork this repository and create a branch named after yourself. Once your done you will need to create a pull request so we can view your changes.

- Screen creative for desktop supplied under the creative folder.

## Commands

In order to run the project you'll need to use the following commands.

npm i

bower i

npm i karma-cli -g

## Notes from the Field

To run:

npm i

bower i

npm i karma-cli -g

grunt

Then host the build folder in a web server and browse to the root of the site.  I just used the build-in server in WebStorm.

To run the unit tests:

npm test


Angular mocks returned an error, which I resolved by this article:
https://rafikitechnology.com/2016/04/26/fixed-issue-with-angular-and-angular-mocks-do-not-agree-jasmine-is-angry/

This is why the angular version is not the same as the one starting version in your source code.

I did not spend a lot of time on formatting and css to be exactly like the example from the creative people.  Since we we're limited in time, I though that was the least important.  It is relatively easy to make it look exact, but time consuming.

Instead I wanted to showcase something that is probably very important and tricky.  Unit testing Angular controller with mocks and promises.  I think this example will separate my code example from most others you will get.
 
Thanks for the opportunity to work on your example and showcase my skills
  
Mark Daunt