describe('Testing the People Controller - uses a Promise', function () {
    var $scope;
    var $q;
    var deferred;

    beforeEach(module('ampApp'));

    beforeEach(inject(function($controller, _$rootScope_, _$q_, peopleService) {
        $q = _$q_;
        $scope = _$rootScope_.$new();

        // We use the $q service to create a mock instance of defer
        deferred = _$q_.defer();

        // Use a Jasmine Spy to return the deferred promise
        spyOn(peopleService, 'getAllPeople').and.returnValue(deferred.promise);

        // Init the controller, passing our spy service instance
        $controller('peopleController', {
            $scope: $scope,
            peopleService: peopleService
        });
    }));

    it('should resolve promise and populate people into the scope', function () {
        // Setup the data we wish to return for the .then function in the controller
        var people = [
            {
                'id': '1',
                'firstName': 'Sean',
                'lastName': 'Kerr',
                'picture': 'img/sean.png',
                'Title': 'Senior Developer'
            },
            {
                'id': '2',
                'firstName': 'Yaw',
                'lastName': 'Ly',
                'picture': 'img/yaw.png',
                'Title': 'AEM Magician'
            }
        ];

        deferred.resolve(people);

        // We have to call the function to populate people into the scope
        $scope.getAllPeople();

        //call apply to ensure the call is completed
        $scope.$apply();

        //now we can perform our assertions
        expect($scope.people).not.toBe(undefined);
        expect($scope.people.length).toEqual(2);
    });
});